import { SubjectService } from './services/subject.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public translate: TranslateService, private subjectService: SubjectService) {

    translate.addLangs(['en', 'ar']);
    translate.setDefaultLang('en');
    this.subjectService.sendClickEvent('en');
    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|ar/) ? browserLang : 'en');
  }

  title = 'startUpPro';

}
