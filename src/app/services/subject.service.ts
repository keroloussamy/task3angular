import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  lang: string;
  constructor() { }
  private subject = new Subject<any>();
  sendClickEvent(lang: string) {
    this.lang = lang;
    this.subject.next();
  }
}
