import { TokenStorageService } from 'src/app/services/token-storage.service';
import { AuthService } from 'src/app/services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private tokenStorageService: TokenStorageService, private router: Router) {


  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    // let url: string = state.url;
    return this.checkUserLogin(route);
  }


  checkUserLogin(route: ActivatedRouteSnapshot): boolean {

    if (this.tokenStorageService.getToken() !== null) {
      return true;
    }
    // this.router.navigate(['/login']);
    return false;
  }
}

