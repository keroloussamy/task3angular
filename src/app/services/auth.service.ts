import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ForgotPasswordDto } from '../Interfaces/forgetPasswordDto.model';
import { ResetPasswordDto } from '../Interfaces/resetPasswordDto.model';

const AUTH_API = 'https://localhost:44322/api/Account/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'login', {
      email,
      password
    }, httpOptions);
  }

  register(firstName: string, lastName: string, firstnameArabic: string, lastnameArabic: string, username: string, email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'register', {
      firstName,
      lastName,
      firstnameArabic,
      lastnameArabic,
      username,
      email,
      password
    }, httpOptions);
  }

  public forgotPassword = (body: ForgotPasswordDto) => {
    return this.http.post(AUTH_API + 'ForgetPassword', body);
  }

  public resetPassword = (body: ResetPasswordDto) => {
    return this.http.post(AUTH_API + 'ResetPassword', body);
  }

}
