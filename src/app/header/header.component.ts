import { SubjectService } from './../services/subject.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedIn = false;
  username?: string;
  userId: string = '';


  constructor(private tokenStorageService: TokenStorageService
    , public translate: TranslateService
    , public subjectService: SubjectService) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    const user = this.tokenStorageService.getUser();
    if (this.isLoggedIn) {
      this.username = user.username;
      this.userId = user.userId;
    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
  OnChange(lang: string) {
    this.translate.use(lang);
    this.subjectService.sendClickEvent(lang);
  }

}

