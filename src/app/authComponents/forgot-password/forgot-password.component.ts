import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ForgotPasswordDto } from 'src/app/Interfaces/forgetPasswordDto.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  public forgotPasswordForm: FormGroup;
  public successMessage: string = '';
  public errorMessage: string = '';
  public showSuccess: boolean = false;
  public showError: boolean = false;

  constructor(private _authService: AuthService) {
    this.forgotPasswordForm = new FormGroup({
      email: new FormControl("", [Validators.required])
    })
  }

  ngOnInit(): void {

  }

  public validateControl = (controlName: string) => {
    return this.forgotPasswordForm.controls[controlName].invalid && this.forgotPasswordForm.controls[controlName].touched
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.forgotPasswordForm.controls[controlName].hasError(errorName)
  }

  public forgotPassword = (forgotPasswordFormValue: any) => {
    this.showError = this.showSuccess = false;

    const forgotPass = { ...forgotPasswordFormValue };
    const forgotPassDto: ForgotPasswordDto = {
      email: forgotPass.email,
      clientURI: 'http://localhost:4200/resetpassword'
    }

    this._authService.forgotPassword(forgotPassDto)
      .subscribe(_ => {
        this.showSuccess = true;
        this.successMessage = 'The link has been sent, please check your email to reset your password.'
      },
        err => {
          this.showError = true;
          this.errorMessage = err;
        })
  }

}
