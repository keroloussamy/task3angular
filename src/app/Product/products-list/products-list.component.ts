import { SubjectService } from './../../services/subject.service';
import { ProductService } from './../../services/product.service';
import { ProductDto } from './../../Interfaces/ProductDTo.model';
import { Component, OnInit } from '@angular/core';
import { AuthGuard } from 'src/app/services/auth.guard';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  products: ProductDto[] = [];

  constructor(private productService: ProductService
    , public authGuards: AuthGuard
    , public subjectService: SubjectService) {

  }

  ngOnInit(): void {
    this.productService.getAllProducts().subscribe(data => { this.products = data; console.log(data) }, err => console.log(err));
  }
  buy() {
    alert('This product is yours now.');
  }
}
