export interface ProductDto {
  id: number
  name: string
  arabicName: string
  price: number
  image: string
  quantity: number
}
